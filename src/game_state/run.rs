use std::usize;

use crate::game_state::player::Player;
use crate::game_state::level::Level;
use crate::game_state::config;
use crate::game_state::combat_log::CombatLog;

pub struct Run {
    pub level_count: usize,
    pub player: Player,
    pub level: Level,
    pub combat_log: CombatLog,
}

impl Run {
    pub fn new(hp: usize) -> Run {
        //let hp = config::PLAYER_STARTING_HP;
        let ap = config::PLAYER_STARTING_AP;
        let weapon = config::PLAYER_STARTING_WEAPON;

        Run {
            level_count: 0,
            player: Player::new(ap, hp, weapon),
            level: Level::new(0),
            combat_log: CombatLog {
                player_action: None,
                damage_taken: None,
                treasure_choice: None,
                level_up: false
            },
        }
    }
}