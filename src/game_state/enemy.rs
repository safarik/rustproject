use tetra::graphics::Color;

pub struct Enemy {
    pub hp: usize,
    pub strength: usize,
    pub weapon_strength: usize,
    pub hit: bool,
}

impl Enemy {
    pub fn new(difficulty: usize) -> Enemy {
        let hp = (difficulty as f32 * 0.5 + 6.0).floor() as usize;
        let strength = (difficulty as f32 * 0.5 + 1.0).floor() as usize;
        let weapon_strength = (difficulty as f32 * 0.66 + 4.0).floor() as usize;

        Enemy {
            hp,
            strength,
            weapon_strength,
            hit: false,
        }
    }
    pub fn coloration(&self)-> Color{
        if self.hit{
            return Color::RED
        }
        return Color::WHITE
    }
}