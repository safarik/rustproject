use tetra::{Context};
use tetra::graphics::{Texture};

pub struct Textures {
    pub tile: Texture,
    pub var_tile: Texture,
    pub hor: Texture,
    pub vert: Texture,
    pub cross: Texture,
    pub player: Texture,
    pub enemy: Texture,
    pub treasure: Texture,
    pub exit: Texture,
    pub title: Texture,
}

impl Textures {
    pub fn init(ctx: &mut Context) -> tetra::Result<Textures> {
        Ok(Textures {
            tile: Texture::new(ctx, "./resources/floor.png")?,
            var_tile: Texture::new(ctx, "./resources/empty.png")?,
            hor: Texture::new(ctx, "./resources/hor3.png")?,
            vert: Texture::new(ctx, "./resources/vert3.png")?,
            cross: Texture::new(ctx, "./resources/cross3.png")?,
            player: Texture::new(ctx, "./resources/player_new.png")?,
            enemy: Texture::new(ctx, "./resources/foe.png")?,
            treasure: Texture::new(ctx, "./resources/chest.png")?,
            exit: Texture::new(ctx, "./resources/stairs.png")?,
            title: Texture::new(ctx, "./resources/title.png")?,
        })
    }
}