pub const WINDOW_WIDTH: f32 = 1920.0;
pub const WINDOW_HEIGHT: f32 = 1080.0;
pub const BOARD_WIDTH: usize = 50;
pub const BOARD_HEIGHT: usize = 28;

pub const PLAYER_STARTING_AP: usize = 1;
pub const PLAYER_STARTING_HP: usize = 10;
pub const PLAYER_STARTING_WEAPON: usize = 6;
pub const CHEST_COUNT: usize = 2;

pub const HEALTH_POTION: usize = 5;
pub const VITALITY: usize = 2;
pub const WEAPON: usize = 1;
pub const STRENGTH: usize = 1;
pub const LEVEL_HEAL: usize = 1;

pub const ROOM_NUMBER: usize = 20;