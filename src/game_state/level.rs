use crate::game_state::{config, Point};
use crate::game_state::enemy::Enemy;
use crate::game_state::tile::{Tile, TreasureType};
use rand::{thread_rng, Rng};
use tetra::math::Vec2;
use std::cmp;
use std::collections::HashMap;
use crate::game_state::tile::TreasureType::{Strength, Vitality, Health};

type Board = [[Tile; config::BOARD_HEIGHT as usize]; config::BOARD_WIDTH as usize];

pub struct Level {
    pub board: Board,
    pub enemies: Vec<Enemy>,
    pub turn_count: usize,
}

impl Level {
    pub fn new(difficulty: usize) -> Level {
        let enemies = Level::create_enemies(difficulty);
        let board = Level::create_board(enemies.len());
        Level {
            board,
            enemies,
            turn_count: 0,
        }
    }

    pub fn get_player_position(&self) -> Point {
        let mut result: Point = (usize::MAX, usize::MAX);

        for x in 0..self.board.len() {
            for y in 0..self.board[x].len() {
                match self.board[x][y] {
                    Tile::Player => {
                        result = (x, y);
                        break;
                    }
                    _ => {}
                }
            }
            if result != (usize::MAX, usize::MAX) {
                break;
            }
        }

        result
    }

    pub fn get_current_enemies(&self) -> HashMap<usize, Point> {
        let mut result: HashMap<usize, Point> = HashMap::new();

        for x in 0..self.board.len() {
            for y in 0..self.board[x].len() {
                let candidate = self.board[x][y];
                match candidate {
                    Tile::Enemy(id) => result.insert(id, (x, y)),
                    _ => Option::None
                };
            }
        }

        result
    }

    pub fn get_tile_at(&self, position: Point) -> Tile {
        self.board[position.0][position.1]
    }

    pub fn move_player_to(&mut self, target: Point) {
        let current = self.get_player_position();
        self.board[current.0][current.1] = Tile::Empty;
        self.board[target.0][target.1] = Tile::Player;
    }

    pub fn move_enemy_to(&mut self, enemy_id: usize, current: Point, target: Point) {
        self.board[current.0][current.1] = Tile::Empty;
        self.board[target.0][target.1] = Tile::Enemy(enemy_id);
    }
}

// ENEMY CREATION
impl Level {
    fn create_enemies(difficulty: usize) -> Vec<Enemy> {
        let enemy_count = Level::calc_enemy_count(difficulty);
        let mut result = Vec::new();

        for _i in 0..enemy_count {
            result.push(Enemy::new(difficulty));
        }
        result
    }

    fn calc_enemy_count(difficulty: usize) -> usize {
        (difficulty as f32 * 1.5) as usize + 5
    }
}

// Board
impl Level {
    fn create_board(enemy_count: usize) -> Board {
        let board = [[Tile::Wall; config::BOARD_HEIGHT]; config::BOARD_WIDTH];
        let with_rooms = Level::add_rooms(board);
        let with_player = Level::add_player(with_rooms);
        let with_enemies = Level::add_enemies(enemy_count, with_player);
        let with_exit = Level::add_exit(with_enemies);
        let with_treasure = Level::add_treasure(with_exit);
        return with_treasure;
    }

    fn add_treasure(board: Board) -> Board {
        let mut board = board.clone();

        for _i in 0..config::CHEST_COUNT {
            board = Level::add_entity(Level::create_treasure_chest(), board);
        }

        board
    }

    fn create_treasure_chest() -> Tile {
        const TREASURE_COUNT: usize = 2;
        let mut treasures = [TreasureType::Health; TREASURE_COUNT];
        for i in 0..TREASURE_COUNT {
            let mut treasure = Level::randomize_treasure();
            if i > 0 {
                while treasure == treasures[i - 1] {
                    treasure = Level::randomize_treasure();
                }
            }
            treasures[i] = treasure;
        }

        Tile::Treasure(treasures[0], treasures[1])
    }

    fn randomize_treasure() -> TreasureType {
        let rnd = thread_rng().gen_range(0, 20);
        match rnd {
            0..=2 => TreasureType::Weapon,
            3..=4 => Strength,
            8..=12 => Vitality,
            _ => Health
        }
    }

    fn add_exit(board: Board) -> Board {
        Level::add_entity(Tile::Exit, board)
    }

    fn add_rooms(board: Board) -> Board {
        let mut board = board.clone();
        let mut rooms: Vec<Room> = Vec::new();
        for _n in 0..config::ROOM_NUMBER {
            let r = Room::new();
            board = r.carve(board);
            rooms.push(r);
        }
        for r in &rooms {
            for r2 in &rooms {
                let p: Path = Path::new(Vec2::new(r.x, r.y), Vec2::new(r2.x, r2.y));
                board = p.carve(board);
            }
        }
        board
    }

    fn add_player(board: Board) -> Board {
        Level::add_entity(Tile::Player, board)
    }

    fn add_enemies(enemy_count: usize, board: Board) -> Board {
        let mut result = board.clone();
        for index in 0..enemy_count {
            result = Level::add_entity(Tile::Enemy(index), result);
        }

        result
    }

    fn add_entity(entity: Tile, board: Board) -> Board {
        let empty_count = Level::count_empty_tiles(board);
        let mut entity_position = thread_rng().gen_range(0, empty_count);


        let mut board = board.clone();
        for x in 0..board.len() {
            for y in 0..board[x].len() {
                if entity_position > 0 {
                    match board[x][y] {
                        Tile::Empty => entity_position -= 1,
                        _ => ()
                    }
                }
                if entity_position == 0 {
                    board[x][y] = entity;
                    return board;
                }
            }
        }

        board
    }

    fn count_empty_tiles(board: Board) -> usize {
        let mut result = 0;
        for x in 0..board.len() {
            for y in 0..board[x].len() {
                match board[x][y] {
                    Tile::Empty => result += 1,
                    _ => {}
                }
            }
        }

        result
    }
}


struct Room {
    x: usize,
    y: usize,
    w: usize,
    h: usize,
}

impl Room {
    fn new() -> Room {
        let x = thread_rng().gen_range(1, config::BOARD_WIDTH - 2);
        let y = thread_rng().gen_range(1, config::BOARD_HEIGHT - 2);
        let w = thread_rng().gen_range(1, cmp::min(6, config::BOARD_WIDTH - x));
        let h = thread_rng().gen_range(1, cmp::min(6, config::BOARD_HEIGHT - y));
        Room { x, y, w, h }
    }
    fn carve(&self, board: Board) -> Board {
        let mut board = board.clone();
        for x in self.x..self.x + self.w {
            for y in self.y..self.y + self.h {
                board[x][y] = Tile::Empty;
            }
        }
        board
    }
}

struct Path {
    start: Vec2<usize>,
    goal: Vec2<usize>,
}

impl Path {
    fn new(start: Vec2<usize>, goal: Vec2<usize>) -> Path {
        Path { start, goal }
    }
    fn carve(&self, board: Board) -> Board {
        let mut board = board.clone();
        for pos in self.plot_path().iter() {
            board[pos.x][pos.y] = Tile::Empty;
        }
        board
    }
    fn plot_path(&self) -> Vec<Vec2<usize>> {
        let mut path: Vec<Vec2<usize>> = Vec::new();
        for x in self.start.x..self.goal.x {
            path.push(Vec2::new(x, self.start.y));
        }
        for y in self.start.y..self.goal.y {
            path.push(Vec2::new(self.goal.x, y));
        }
        path
    }
}
