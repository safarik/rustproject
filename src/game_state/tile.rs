#[derive(Debug, Copy, Clone)]
pub enum Tile {
    Enemy(usize),
    Exit,
    Empty,
    Treasure(TreasureType, TreasureType),
    Player,
    Wall,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TreasureType {
    Health,
    Strength,
    Weapon,
    Vitality
}