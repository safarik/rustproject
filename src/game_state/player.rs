pub struct Player {
    pub current_hp: usize,
    pub max_hp: usize,
    pub strength: usize,
    pub weapon: usize,
    pub current_xp: usize,
    pub max_xp: usize,
    pub orientation: f32,
}

impl Player {
    pub fn new(strength: usize, hit_points: usize, weapon: usize) -> Player {
        Player {
            current_hp: hit_points,
            max_hp: hit_points,
            strength,
            weapon,
            current_xp: 0,
            max_xp: 5,
            orientation: 1.0,
        }
    }
}