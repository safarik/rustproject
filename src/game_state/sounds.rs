use tetra::audio::Sound;

pub struct Sounds {
    pub music: Sound,
    pub hit: Sound,
    pub fanfare: Sound,
    pub gameover: Sound,
    pub exit: Sound,
    pub chest: Sound,
    pub kill: Sound,
}

impl Sounds {
    pub fn init() -> tetra::Result<Sounds> {
        Ok(Sounds {
            music: Sound::new("./resources/music.mp3")?,
            hit: Sound::new("./resources/hit.wav")?,
            fanfare: Sound::new("./resources/fanfare.wav")?,
            gameover: Sound::new("./resources/gameover.wav")?,
            exit: Sound::new("./resources/exit.mp3")?,
            chest: Sound::new("./resources/chest.mp3")?,
            kill: Sound::new("./resources/kill.wav")?,
        })
    }
}