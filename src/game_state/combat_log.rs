use crate::game_state::tile::TreasureType;

#[derive(Debug)]
pub struct CombatLog {
    pub player_action: Option<PlayerAction>,
    pub damage_taken: Option<usize>,
    pub treasure_choice: Option<(TreasureType, TreasureType)>,
    pub level_up: bool
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PlayerAction {
    Move,
    Wait,
    Level,
    Attack(usize, usize), // damage, remaining hp
}