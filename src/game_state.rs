mod run;
mod level;
mod player;
mod enemy;
mod textures;
pub mod config;
mod tile;
mod combat_log;
mod sounds;

use tetra::audio::SoundInstance;
use tetra::{Context, State};
use tetra::graphics::{self, Color, DrawParams, Texture};
use tetra::input::{self, Key};
use run::Run;
use textures::Textures;
use crate::game_state::tile::{Tile, TreasureType};
use tetra::math::Vec2;
use std::cmp::{min, max};
use std::usize;
use rand::{thread_rng, Rng};
use tetra::graphics::text::{VectorFontBuilder, Font, Text};
use crate::game_state::combat_log::PlayerAction;
use crate::game_state::combat_log::PlayerAction::{Move, Wait};
use crate::game_state::level::Level;
use crate::game_state::sounds::Sounds;

type Point = (usize, usize);

pub struct GameState {
    current_run: Run,
    high_score: usize,
    textures: Textures,
    sounds: Sounds,
    font: Font,
    font_huge: Font,
    music: Option<SoundInstance>,
}

impl GameState {
    pub fn new(ctx: &mut Context) -> tetra::Result<GameState> {
        let textures = Textures::init(ctx);
        let font = VectorFontBuilder::new("./resources/corbel.ttf")?.with_size(ctx, 16.0);
        let huge = VectorFontBuilder::new("./resources/corbel.ttf")?.with_size(ctx, 50.0);

        let sounds = Sounds::init();
        let mut music = None;
        if let Result::Ok(loaded_music) = sounds.as_ref().unwrap().music.repeat_with(ctx, 0.2, 1.0) {
            music = Option::from(loaded_music);
        }

        if textures.is_ok() {
            if font.is_ok() && huge.is_ok() {
                Ok(GameState {
                    current_run: Run::new(0),
                    high_score: 0,
                    textures: textures.unwrap(),
                    sounds: sounds.unwrap(),
                    font: font.unwrap(),
                    font_huge: huge.unwrap(),
                    music,
                })
            } else {
                Err(font.err().unwrap())
            }
        } else {
            Err(textures.err().unwrap())
        }
        
    }
}

impl State for GameState {
    fn update(&mut self, ctx: &mut Context) -> tetra::Result {
        if self.handle_user_turn(ctx) {
            self.run_enemy_turn(ctx);
            self.finish_turn();
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> tetra::Result {
        graphics::clear(ctx, Color::rgb(0.220, 0.220, 0.220));

        if self.current_run.player.current_hp > 0 {
            self.draw_board(ctx);
        } else {
            self.draw_game_over(ctx);
        }
        self.draw_stats(ctx);
        self.draw_combat_log(ctx);

        Ok(())
    }
}

// SOUNDS

impl GameState {
    fn play_sound_player_attack(&mut self, ctx: &mut Context) {
        if let Err(error) = self.sounds.hit.play(ctx) {
            println!("{}", error);
        }
    }

    fn play_sound_enemy_attack(&mut self, ctx: &mut Context) {
        self.play_sound_player_attack(ctx);
    }

    fn play_sound_killed_enemy(&mut self, ctx: &mut Context) {
        if let Err(error) = self.sounds.kill.play(ctx) {
            println!("{}", error);
        }
    }

    fn play_sound_death(&mut self, ctx: &mut Context) {
        if let Err(error) = self.sounds.gameover.play(ctx) {
            println!("{}", error);
        }
    }

    fn play_sound_chest(&mut self, ctx: &mut Context) {
        if let Err(error) = self.sounds.chest.play(ctx) {
            println!("{}", error);
        }
    }

    fn play_sound_level_up(&mut self, ctx: &mut Context) {
        if let Err(error) = self.sounds.fanfare.play(ctx) {
            println!("{}", error);
        }
    }

    fn play_sound_exit(&mut self, ctx: &mut Context) {
        if let Err(error) = self.sounds.exit.play(ctx) {
            println!("{}", error);
        }
    }
}

// USER TURN

impl GameState {
    /**
        RETURNS:
            true <-> user took their turn -> continue with enemy turn!
            false <-> user didn't do anything or produced an error (moved into wall, etc)
     */
    fn handle_user_turn(&mut self, ctx: &mut Context) -> bool {
        let key = GameState::get_pressed_key(ctx);

        let is_player_alive = self.current_run.player.current_hp > 0;
        let is_treasure_choice = self.current_run.combat_log.treasure_choice != None;

        return match key {
            Key::CapsLock => false,
            Key::M => self.toggle_music(ctx),
            Key::N => !is_player_alive && self.create_new_run(),
            Key::W | Key::A | Key::S | Key::D | Key::Down | Key::Up | Key::Right | Key::Left => {
                self.current_run.combat_log.level_up = false;
                
                if is_player_alive && !is_treasure_choice{
                    let moved = self.handle_user_movement(key, ctx);
                    if moved{
                        match key{
                            Key::A => self.current_run.player.orientation = -1.0,
                            Key::D => self.current_run.player.orientation = 1.0,
                            _ => {}
                        }
                    }
                    return moved;
                }
                else {
                    return false;
                }
            }
            Key::Space => {
                self.current_run.combat_log.level_up = false;
                return is_player_alive && !is_treasure_choice && self.do_wait();
            }
            Key::Num1 | Key::Num2 | Key::NumPad1 | Key::NumPad2 =>
                is_player_alive && is_treasure_choice && self.handle_treasure_choice(key),
            _ => false
        };
    }

    fn toggle_music(&mut self, ctx: &mut Context) -> bool {
        if let Some(sound) = &mut self.music {
            sound.stop();
            self.music = None;
        } else if let Result::Ok(loaded_music) = self.sounds.music.repeat_with(ctx, 0.2, 1.0) {
            self.music = Option::from(loaded_music);
        }
        false
    }

    fn handle_treasure_choice(&mut self, key: Key) -> bool {
        if let Some(treasure) = self.current_run.combat_log.treasure_choice {
            match key {
                Key::Num1 | Key::NumPad1 => self.take_treasure(treasure.0),
                Key::Num2 | Key::NumPad2 => self.take_treasure(treasure.1),
                _ => ()
            }
        }
        true
    }

    fn take_treasure(&mut self, treasure: TreasureType) {
        match treasure {
            TreasureType::Health => {
                let potential_new_hp = self.current_run.player.current_hp + config::HEALTH_POTION;
                let new_hp = min(potential_new_hp, self.current_run.player.max_hp);
                self.current_run.player.current_hp = new_hp;
            }
            TreasureType::Strength => self.current_run.player.strength += config::STRENGTH,
            TreasureType::Weapon => self.current_run.player.weapon += config::WEAPON,
            TreasureType::Vitality => self.current_run.player.max_hp += config::VITALITY
        }
        self.current_run.combat_log.treasure_choice = None;
    }

    fn do_wait(&mut self) -> bool {
        self.current_run.combat_log.player_action = Option::from(Wait);
        true
    }

    fn create_new_run(&mut self) -> bool {
        self.current_run = Run::new(config::PLAYER_STARTING_HP);
        false
    }

    fn handle_user_movement(&mut self, key: Key, ctx: &mut Context) -> bool {
        let player_position = self.current_run.level.get_player_position();
        let target_position = GameState::calculate_target_position(player_position, key);

        if player_position == target_position{
            return false;
        }

        let target_tile = self.current_run.level.get_tile_at(target_position);
        match target_tile {
            Tile::Empty => {
                self.current_run.level.move_player_to(target_position);
                self.current_run.combat_log.player_action = Option::from(Move);
            }
            Tile::Wall => return false,
            Tile::Enemy(id) => self.attack_enemy(id, target_position, ctx),
            Tile::Exit => return self.progress_level(ctx),
            Tile::Treasure(a, b) => return self.open_chest(a, b, target_position, ctx),
            _ => ()
        }
        true
    }

    fn open_chest(&mut self, option_a: TreasureType, option_b: TreasureType, position: Point, ctx: &mut Context) -> bool {
        self.current_run.level.move_player_to(position);
        self.current_run.combat_log.treasure_choice = Option::from((option_a, option_b));
        self.play_sound_chest(ctx);
        false
    }

    fn progress_level(&mut self, ctx: &mut Context) -> bool {
        self.current_run.level_count += 1;
        if self.current_run.player.current_hp < self.current_run.player.max_hp {
            self.current_run.player.current_hp += config::LEVEL_HEAL;
        }
        self.current_run.level = Level::new(self.current_run.level_count);
        self.current_run.combat_log.player_action = Option::from(PlayerAction::Level);
        self.play_sound_exit(ctx);
        false
    }

    fn attack_enemy(&mut self, enemy_id: usize, enemy_position: Point, ctx: &mut Context) {
        let damage = self.current_run.player.strength + GameState::roll_dice(self.current_run.player.weapon);

        if let Some(enemy) = self.current_run.level.enemies.get_mut(enemy_id) {
            if enemy.hp <= damage {
                self.current_run.level.move_player_to(enemy_position);
                self.current_run.combat_log.player_action = Option::from(PlayerAction::Attack(damage, 0));
                self.play_sound_killed_enemy(ctx);
                self.grant_xp(ctx);
            } else {
                enemy.hp -= damage;
                enemy.hit = true;
                self.current_run.combat_log.player_action = Option::from(PlayerAction::Attack(damage, enemy.hp));
                self.play_sound_player_attack(ctx);
            }
        }
    }

    fn grant_xp(&mut self, ctx: &mut Context) {
        self.current_run.player.current_xp += 1;
        if self.current_run.player.current_xp == self.current_run.player.max_xp {
            self.current_run.player.strength += 1;
            self.current_run.player.weapon += 1;
            self.current_run.player.max_hp += 1;
            self.current_run.player.current_hp = self.current_run.player.max_hp;
            self.current_run.player.max_xp += 2;
            self.current_run.player.current_xp = 0;
            self.current_run.combat_log.level_up = true;
            self.play_sound_level_up(ctx);
        }
    }

    fn roll_dice(sides: usize) -> usize {
        thread_rng().gen_range(1, sides)
    }

    fn calculate_target_position(current_position: Point, key: Key) -> Point {
        match key {
            Key::W | Key::Up => {
                if current_position.1 == 0 {
                    current_position
                } else {
                    (current_position.0, current_position.1 - 1)
                }
            }
            Key::A | Key::Left => {
                if current_position.0 == 0 {
                    current_position
                } else {
                    (current_position.0 - 1, current_position.1)
                }
            }
            Key::S | Key::Down => (current_position.0, min(current_position.1 + 1, config::BOARD_HEIGHT - 1)),
            Key::D | Key::Right => (min(current_position.0 + 1, config::BOARD_WIDTH - 1), current_position.1),
            _ => current_position
        }
    }

    fn get_pressed_key(ctx: &mut Context) -> Key {
        let keys = input::get_keys_pressed(ctx);
        let mut result = input::Key::CapsLock;
        for key in keys {
            result = *key
        }
        result
    }
}

// ENEMY TURN

impl GameState {
    fn run_enemy_turn(&mut self, ctx: &mut Context) {
        let player_position = self.current_run.level.get_player_position();

        let mut total_damage = 0;

        for (id, position) in self.current_run.level.get_current_enemies() {
            if GameState::check_is_neighbor(player_position, position) {
                total_damage += self.attack_player(id, ctx);
            } else {
                self.move_enemy(id, position);
            }
        }

        if total_damage > 0 {
            self.current_run.combat_log.damage_taken = Option::from(total_damage)
        } else {
            self.current_run.combat_log.damage_taken = None
        }

        if self.current_run.player.current_hp == 0 {
            self.finish_run();
        }
    }

    fn finish_run(&mut self) {
        self.high_score = max(self.high_score, self.current_run.level_count);
    }

    fn check_is_neighbor(a: Point, b: Point) -> bool {
        GameState::check_neighbor_uint(a.0, b.0) && a.1 == b.1
            || GameState::check_neighbor_uint(a.1, b.1) && a.0 == b.0
    }

    fn check_neighbor_uint(a: usize, b: usize) -> bool {
        if a > b {
            if a - b == 1 {
                return true;
            }
        } else if b > a {
            if b - a == 1 {
                return true;
            }
        }
        false
    }

    fn attack_player(&mut self, enemy_id: usize, ctx: &mut Context) -> usize {
        let mut damage = 0;

        if let Some(enemy) = self.current_run.level.enemies.get(enemy_id) {
            damage = enemy.strength + GameState::roll_dice(enemy.weapon_strength);
            if self.current_run.player.current_hp <= damage {
                self.current_run.player.current_hp = 0;
                self.play_sound_death(ctx);
            } else {
                self.current_run.player.current_hp -= damage;
                self.play_sound_enemy_attack(ctx);
            }
        }

        damage
    }

    fn move_enemy(&mut self, id: usize, position: Point) {
        let mut target_position = GameState::randomize_target_position(position);
        let mut hacky_while_counter = 0;
        while !self.validate_target_position(target_position) {
            target_position = GameState::randomize_target_position(position);
            hacky_while_counter += 1;
            if hacky_while_counter > 20 {
                return;
            }
        }
        self.current_run.level.move_enemy_to(id, position, target_position);
    }

    fn randomize_target_position(position: Point) -> Point {
        match thread_rng().gen_range(0, 5) {
            0 => {
                if position.1 == 0 {
                    position
                } else {
                    (position.0, position.1 - 1)
                }
            }
            1 => {
                if position.0 == 0 {
                    position
                } else {
                    (position.0 - 1, position.1)
                }
            }
            2 => (position.0, min(position.1 + 1, config::BOARD_HEIGHT - 1)),
            3 => (min(position.0 + 1, config::BOARD_WIDTH - 1), position.1),
            _ => position
        }
    }

    fn validate_target_position(&self, position: Point) -> bool {
        match self.current_run.level.get_tile_at(position) {
            Tile::Empty => true,
            _ => false
        }
    }
}

impl GameState {
    fn finish_turn(&mut self) {
        self.current_run.level.turn_count += 1;
    }
}

// DRAWING HELPERS

impl GameState {
    fn draw_game_over(&mut self, ctx: &mut Context) {
        
        self.textures.title.draw(ctx, DrawParams{
            position: Vec2::new(0.0,0.0),
            scale: Vec2::new(1.0,1.0),
            origin: Vec2::new(0.0,0.0),
            rotation: 0.0,
            color: Color::WHITE,
        });
        Text::new("For a new run, press 'N'!", self.font_huge.clone())
            .draw(ctx, Vec2::new(700.0, 950.0));
        //self.draw_string("For a new run, press 'N'!".parse().unwrap(), 550.0, 50.0, ctx);

    }

    fn draw_board(&mut self, ctx: &mut Context) {
        GameState::draw_boundary(self, ctx);
        GameState::draw_tiles(self, ctx);
    }

    fn draw_boundary(&mut self, ctx: &mut Context) {
        let boundary_width = config::BOARD_WIDTH + 2;
        let boundary_height = config::BOARD_HEIGHT + 2;

        for x in 0..boundary_width {
            GameState::draw_border_tile(self, Color::rgb(0.5, 0.5, 0.5), x, 0, ctx);
            GameState::draw_border_tile(self, Color::rgb(0.5, 0.5, 0.5), x, boundary_height - 1, ctx);
        }
        for y in 0..boundary_height {
            GameState::draw_border_tile(self, Color::rgb(0.5, 0.5, 0.5), 0, y, ctx);
            GameState::draw_border_tile(self, Color::rgb(0.5, 0.5, 0.5), boundary_width - 1, y, ctx);
        }
    }

    fn draw_border_tile(&mut self, color: Color, x: usize, y: usize, ctx: &mut Context) {
        self.draw_texture(&self.textures.cross, x, y, Vec2::new(1.0,1.0), 0.0, color, ctx);
    }

    fn draw_tiles(&mut self, ctx: &mut Context) {
        let board = self.current_run.level.board;

        for x in 0..board.len() {
            for y in 0..board[x].len() {
                let tile = board[x][y];
                let draw_x = x + 1;
                let draw_y = y + 1;
                match tile {
                    Tile::Empty => GameState::draw_tile(self, Color::WHITE, draw_x, draw_y, ctx),
                    Tile::Wall => GameState::draw_wall(self, draw_x, draw_y, ctx),
                    Tile::Player => GameState::draw_player(self, draw_x, draw_y, ctx),
                    Tile::Enemy(id) => {
                        let mut color = Color::WHITE;
                        if let Some(enemy) =  self.current_run.level.enemies.get_mut(id) {
                            color = enemy.coloration();
                            enemy.hit = false;
                        }
                        GameState::draw_enemy(self, draw_x, draw_y, ctx, color);},
                    Tile::Exit => GameState::draw_exit(self, draw_x, draw_y, ctx),
                    Tile::Treasure(_, _) => GameState::draw_treasure(self, draw_x, draw_y, ctx),
                }
            }
        }
    }

    fn draw_exit(&mut self, x: usize, y: usize, ctx: &mut Context) {
        GameState::draw_tile(self, Color::WHITE, x, y, ctx);

        self.draw_texture(&self.textures.exit, x, y, Vec2::new(1.0,1.0), 0.0, Color::WHITE, ctx);
    }

    fn draw_texture(&self, texture: &Texture, x: usize, y: usize, scale: Vec2<f32>, inset: f32, color: Color, ctx: &mut Context) {
        texture.draw(ctx, DrawParams {
            position: Vec2::new(inset + (x as f32 * 33.0), inset + (y as f32 * 33.0)),
            scale: Vec2::new(scale.x, scale.y),
            origin: Vec2::new(inset, inset),
            rotation: 0.0,
            color,
        })
    }

    fn draw_treasure(&mut self, x: usize, y: usize, ctx: &mut Context) {
        GameState::draw_tile(self, Color::WHITE, x, y, ctx);

        self.draw_texture(&self.textures.treasure, x, y, Vec2::new(1.0,1.0), 0.0, Color::WHITE, ctx);
    }

    fn draw_tile(&mut self, color: Color, x: usize, y: usize, ctx: &mut Context) {
        self.draw_texture(&self.textures.tile, x, y, Vec2::new(1.0,1.0), 0.0, color, ctx);
    }

    fn draw_wall(&mut self, x: usize, y: usize, ctx: &mut Context) {
        let mut color = Color::WHITE;
        let val = self.calc_wall_type(x - 1, y - 1);
        let mut tex: Texture = self.textures.cross.clone();
        match val {
            11110001 | 11111 | 10001 | 10011 | 11001 | 110001 | 10010001 | 11011 | 110011 | 10010011 | 111001 | 10011001 | 10110001 | 111011 | 10011011 | 10110011 | 10111001 | 10111011 | 11111001 | 11110011 | 11111011 | 10011111 | 111111 | 10111111 => tex = self.textures.hor.clone(),
            1000100 | 11000111 | 1111100 | 1000110 | 1001100 | 1100100 | 11000100 | 1001110 | 1100110 | 11000110 | 1101100 | 11001100 | 11100100 | 1101110 | 11001110 | 11100110 | 11101100 | 11101110 | 11100111 | 11001111 | 11101111 | 11111100 | 1111110 | 11111110 => tex = self.textures.vert.clone(),
            11111111 => {
                tex = self.textures.var_tile.clone();
                color = Color::rgb(0.5, 0.5, 0.5);
            }
            _ => {}
        }
        self.draw_texture(&tex, x, y, Vec2::new(1.0,1.0), 0.0, color, ctx);
    }

    fn calc_wall_type(&self, x: usize, y: usize) -> u32 {
        let board = self.current_run.level.board;
        let mut result: u32 = 0;


        if x == 0 || y == 0 {
            result += 10000000;
        } else {
            match board[x - 1][y - 1] {
                Tile::Wall => result += 10000000,
                _ => {}
            }
        }
        if y == 0 {
            result += 1000000;
        } else {
            match board[x][y - 1] {
                Tile::Wall => result += 1000000,
                _ => {}
            }
        }
        if x >= config::BOARD_WIDTH - 1 || y == 0 {
            result += 100000;
        } else {
            match board[x + 1][y - 1] {
                Tile::Wall => result += 100000,
                _ => {}
            }
        }
        if x >= config::BOARD_WIDTH - 1 {
            result += 10000;
        } else {
            match board[x + 1][y] {
                Tile::Wall => result += 10000,
                _ => {}
            }
        }
        if x >= config::BOARD_WIDTH - 1 || y >= config::BOARD_HEIGHT - 1 {
            result += 1000;
        } else {
            match board[x + 1][y + 1] {
                Tile::Wall => result += 1000,
                _ => {}
            }
        }
        if y >= config::BOARD_HEIGHT - 1 {
            result += 100;
        } else {
            match board[x][y + 1] {
                Tile::Wall => result += 100,
                _ => {}
            }
        }
        if x == 0 || y >= config::BOARD_HEIGHT - 1 {
            result += 10;
        } else {
            match board[x - 1][y + 1] {
                Tile::Wall => result += 10,
                _ => {}
            }
        }
        if x == 0 {
            result += 1;
        } else {
            match board[x - 1][y] {
                Tile::Wall => result += 1,
                _ => {}
            }
        }

        result
    }

    fn draw_player(&mut self, x: usize, y: usize, ctx: &mut Context) {
        GameState::draw_tile(self, Color::WHITE, x, y, ctx);

        self.draw_texture(&self.textures.player, x, y, Vec2::new(1.15*self.current_run.player.orientation,1.15), 16.0, Color::WHITE, ctx);
    }

    fn draw_enemy(&mut self, x: usize, y: usize, ctx: &mut Context, color :Color) {
        GameState::draw_tile(self, Color::WHITE, x, y, ctx);

        self.draw_texture(&self.textures.enemy, x, y, Vec2::new(1.15,1.15), 14.0, color, ctx);
    }

    fn draw_stats(&self, ctx: &mut Context) {
        if self.current_run.player.current_hp <= 0{
            return 
        }
        let text_top = GameState::calculate_text_top();
        let stats_string = format!(
            "Health: {} / {}        Damage: {} + 1d{}",
            self.current_run.player.current_hp,
            self.current_run.player.max_hp,
            self.current_run.player.strength,
            self.current_run.player.weapon
        );
        let turn_string = format!(
            "Turn: {}        XP: {} / {}",
            self.current_run.level.turn_count,
            self.current_run.player.current_xp,
            self.current_run.player.max_xp
        );
        let level_string = format!("Level: {}       High Score: {}", self.current_run.level_count, self.high_score);

        self.draw_string(stats_string, 50.0, text_top, ctx);
        self.draw_string(turn_string, 50.0, text_top + 25.0, ctx);
        self.draw_string(level_string, 50.0, text_top + 50.0, ctx);
    }

    fn draw_combat_log(&self, ctx: &mut Context) {
        if self.current_run.player.current_hp <= 0{
            return 
        }
        if let Some(treasure_choice) = self.current_run.combat_log.treasure_choice {
            self.draw_treasure_choice(treasure_choice.0, treasure_choice.1, ctx);
        } else {
            self.draw_player_action(ctx);
            self.draw_enemy_action(ctx);
            self.draw_level_up(ctx);
        }
    }

    fn draw_treasure_choice(&self, option_a: TreasureType, option_b: TreasureType, ctx: &mut Context) {
        let text_x = 500.0;
        let text_top = GameState::calculate_text_top();

        let a = GameState::treasure_string_for(option_a);
        let b = GameState::treasure_string_for(option_b);
        let str = format!("You have found some treasure. Choose between:\n  [ 1 ] {}      or\n    [ 2 ] {}", a, b);
        self.draw_string(str, text_x, text_top, ctx);
    }

    fn treasure_string_for(treasure: TreasureType) -> String {
        match treasure {
            TreasureType::Health => format!("a health potion (+{} HP)", config::HEALTH_POTION),
            TreasureType::Vitality => format!("some melange spice (+{} max HP)", config::VITALITY),
            TreasureType::Weapon => format!("a sharpening stone (+{} max damage)", config::WEAPON),
            TreasureType::Strength => format!("some steroids (+{} min damage)", config::STRENGTH),
        }
    }

    fn draw_player_action(&self, ctx: &mut Context) {
        let text_x = 500.0;
        let text_top = GameState::calculate_text_top();
        match self.current_run.combat_log.player_action {
            Some(PlayerAction::Level) => {
                let str = format!("You reached level {} and healed +{} HP.", self.current_run.level_count, config::LEVEL_HEAL);
                self.draw_string(str, text_x, text_top, ctx);
            }
            Some(PlayerAction::Move) =>
                self.draw_string("You moved.".parse().unwrap(), text_x, text_top, ctx),
            Some(PlayerAction::Wait) =>
                self.draw_string("You waited.".parse().unwrap(), text_x, text_top, ctx),
            Some(PlayerAction::Attack(damage_done, remaining_hp)) =>
                self.draw_string(
                    format!("You attacked for {} damage. The enemy has {} hit points remaining.", damage_done, remaining_hp),
                    text_x,
                    text_top,
                    ctx,
                ),
            None => ()
        };
    }

    fn draw_enemy_action(&self, ctx: &mut Context) {
        let text_top = GameState::calculate_text_top() + 25.0;
        let text_x = 500.0;
        if let Some(PlayerAction::Level) = self.current_run.combat_log.player_action {
            let hp = self.current_run.level.enemies[0].hp;
            let wep = self.current_run.level.enemies[0].weapon_strength;
            let str = self.current_run.level.enemies[0].strength;
            self.draw_string(format!("Enemies now have {} HP and do {} + 1d{} damage", hp, str, wep), text_x, text_top, ctx);
        } else if let Some(damage) = self.current_run.combat_log.damage_taken {
            self.draw_string(format!("The enemy attacked you for {} damage.", damage), text_x, text_top, ctx);
        };
    }

    fn draw_level_up(&self, ctx: &mut Context) {
        if self.current_run.combat_log.level_up {
            let text_top = GameState::calculate_text_top() + 50.0;
            let text_x = 500.0;
            let str = "You have gained enough experience to level up. Your stats increased by 1 and you're fully healed!";
            self.draw_string(str.parse().unwrap(), text_x, text_top, ctx);
        }
    }

    fn draw_string(&self, string: String, x: f32, y: f32, ctx: &mut Context) {
        Text::new(string, self.font.clone())
            .draw(ctx, Vec2::new(x, y));
    }

    fn calculate_text_top() -> f32 {
        33.0 * (config::BOARD_HEIGHT as f32 + 2.0) + 7.0
    }
}
