mod game_state;

use tetra::ContextBuilder;

use crate::game_state::GameState;
use crate::game_state::config;

fn main() -> tetra::Result {
    ContextBuilder::new("rusttest", config::WINDOW_WIDTH as i32, config::WINDOW_HEIGHT as i32)
        .quit_on_escape(true)
        .build()?
        .run(GameState::new)
}
